#include <iostream>
#include <systemc.h>
#include "connections.hpp"

using namespace std;

typedef sc_signal<int>  ch1;
typedef sc_signal<int>  ch2;
typedef sc_signal<int>  ch3;
typedef sc_signal<int>  ch4;

int sc_main(int argc, char* argv[]) 
{
  cout << "INFO: Elaborating " << endl;
  
  ch1 cp1i("cp1i");
  ch2 cp2i("cp2i");
  ch3 cp3i("cp3i");
  ch4 cp4i("cp4i");

  Top top("top");
  Peer peer("peer");
  
  top.p1(cp1i);
  top.p2(cp2i);
  top.p3(cp3i);
  top.p4(cp4i);
  peer.pJ(top.p5);
  
  cout << "INFO: Simulating " << endl;
  sc_start();
  
  cout << "INFO: Post-processing " << endl;

  return 0;
}
