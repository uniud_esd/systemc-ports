#include <systemc.h>
#include <string>
#include "export.hpp"

using namespace std;

SC_MODULE(TestBench) {
    sc_signal<unsigned> injecter;
    sc_fifo<unsigned> observer;

    ExportOuterModule outer1_i;
    
    SC_CTOR(TestBench) : outer1_i("outer1_i")
    {
        SC_THREAD(stimulus_thread);
        SC_THREAD(observer_thread);
          sensitive << outer1_i.dout;
        outer1_i.din(injecter);
        init_values();
    }

    int check() {

        for (unsigned i=0;i<TEST_SIZE;i++) {
            if (observer.read() != (1<<(1<<values[i])))
                return 1;
        }
        if (sc_time_stamp() != sc_time(10*TEST_SIZE,SC_MS))
            return 1;                   

        return 0;
    }

  private:

    void stimulus_thread() {

        for (unsigned i=0;i<TEST_SIZE;i++) {
            injecter.write(values[i]);    
            wait(10,SC_MS);
        }
    }

    void observer_thread() {
        while(true) {
            wait();
            unsigned value = outer1_i.dout->read();
            cout << "observer_thread: at " << sc_time_stamp() << " read value " << value << endl;
            observer.write(value);
        }        
    }

    static const unsigned TEST_SIZE = 3;
    int values[TEST_SIZE];
    
    void init_values() {
        values[0] = 4;
        values[1] = 2;
        values[2] = 3;
    }
};

int sc_main(int argc, char** argv) {

  TestBench test("test");

  sc_start();

  return test.check();
}

