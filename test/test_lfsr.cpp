#include <systemc.h>
#include <string>
#include "lfsr.hpp"

using namespace std;

SC_MODULE(TestBench) {

    sc_signal<bool> sample;
    sc_signal<bool> clock;
    sc_signal<bool> reset;
    sc_signal<sc_uint<32> > signature;

    LFSR lfsr_i;
    
    SC_CTOR(TestBench) : lfsr_i("lfsr_i")
    {
        SC_THREAD(stimulus_thread);
        SC_THREAD(clock_thread);
        SC_THREAD(watcher_thread);
            sensitive << clock.posedge_event();
        lfsr_i.sample(this->sample);
        lfsr_i.clock(this->clock);
        lfsr_i.reset(this->reset);
        lfsr_i.signature(this->signature);
    }

    int check() {

        if (153 != signature.read())
            return 1;                   
      
        return 0;
    }

  private:

    void clock_thread() {

        bool value = false;

        while(true) {
            clock.write(value);
            value = !value;
            wait(5,SC_NS);
        }
    }

    void stimulus_thread() {
        reset.write(false);
        sample.write(true);
        wait(15,SC_NS);
        sample.write(false);
        wait(30,SC_NS);
        reset.write(true);
        wait(5,SC_NS);
        reset.write(false);
        sample.write(true);
    }

    void watcher_thread() {

        while(true) {
            wait();
            cout << sc_time_stamp() << ": " << signature << endl;
        }
    }
};

int sc_main(int argc, char** argv) {

  TestBench test("test");

  sc_start(120,SC_NS);

  return test.check();
}

