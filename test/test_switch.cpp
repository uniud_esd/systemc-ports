#include <iostream>
#include <systemc.h>
#include "switch.hpp"

SC_MODULE(TestBench) 
{
  Switch switch_i;
  sc_fifo<int> t1A, t1B, t1C, t1D;
  sc_signal<bool> request[9];
  
  SC_CTOR(TestBench) : switch_i("switch_i") {
    switch_i.T1_ip(t1A); 
    switch_i.T1_ip(t1B);
    switch_i.T1_ip(t1C); 
    switch_i.T1_ip(t1D);
    for (unsigned i=0;i!=9;i++) {
      switch_i.request_op(request[i]);
    }
    SC_THREAD(stimulus_thread);
  }
  
  void stimulus_thread() {
      wait(SC_ZERO_TIME);
      for (unsigned i=0;i!=9;i++) {
        int val;
        wait(1,SC_NS);
        val = rand();
        switch(request[i].read()?i:9-i) {
            case 0:
            case 7:
                cout << "INFO(writer_thread): "
                     << "Writing 0x" << hex << val 
                     << " to t1A "
                     << "at " << sc_time_stamp()
                     << endl;
                t1A.write(val); 
                break;
            case 1:
            case 6:
                cout << "INFO(writer_thread): "
                     << "Writing 0x" << hex << val 
                     << " to t1B "
                     << "at " << sc_time_stamp()
                     << endl;
                t1B.write(val); 
                break;
            case 2:
            case 5:
            case 9:
                cout << "INFO(writer_thread): "
                     << "Writing 0x" << hex << val 
                     << " to t1C "
                     << "at " << sc_time_stamp()
                     << endl;
                t1C.write(val); 
                break;
            case 3:
            case 4:
                cout << "INFO(writer_thread): "
                     << "Writing 0x" << hex << val 
                     << " to t1D "
                     << "at " << sc_time_stamp()
                     << endl;
                t1D.write(val); 
                break;
        }
      } 
  }
};

int sc_main(int argc, char* argv[]) 
{
  cout << "INFO: Elaborating " << endl;
  TestBench test_i("test_i");
  
  cout << "INFO: Simulating "<< endl;
  sc_start();

  cout << "INFO: Post-processing "<< endl;

  return 0;
}
