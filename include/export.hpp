#ifndef EXPORT_HPP
#define EXPORT_HPP

SC_MODULE(ExportInnerModule) {

    sc_port<sc_signal_inout_if<unsigned> > din;
    sc_export<sc_signal_inout_if<unsigned> > dout;
    sc_signal<unsigned> dout_ch;

    SC_CTOR(ExportInnerModule) : din("din"), dout("dout") {
      SC_THREAD(elevate_thread);
        sensitive << din;
      dout(dout_ch);
    }

  private:
    
    void elevate_thread();
};

SC_MODULE(ExportOuterModule) {

    sc_port<sc_signal_inout_if<unsigned> > din;
    sc_export<sc_signal_inout_if<unsigned> > dout;
    
    ExportInnerModule inner1, inner2;
    
    SC_CTOR(ExportOuterModule) : din("din"), dout("dout"), inner1("inner1"), inner2("inner2") {
        inner1.din(din);
        inner2.din(inner1.dout);
        dout(inner2.dout);
    }
};


#endif 

