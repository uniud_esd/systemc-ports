#ifndef LFSR_HPP
#define LFSR_HPP

#include <systemc.h>

SC_MODULE(LFSR) 
{
  sc_in<bool> sample;
  sc_out<sc_uint<32> > signature;
  sc_in<bool> clock;
  sc_in<bool> reset;
  
  SC_CTOR(LFSR) 
  {
    SC_METHOD(shift_method);
      sensitive << clock.pos() << reset;
      signature.initialize(0);
  }
  
private:

  void shift_method();
  sc_uint<32> LFSR_reg;
};

#endif
