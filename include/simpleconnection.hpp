#ifndef SIMPLECONNECTION_HPP
#define SIMPLECONNECTION_HPP

SC_MODULE(InnerModule) {

    sc_port<sc_fifo_in_if<unsigned> > din;
    sc_port<sc_fifo_out_if<unsigned> > dout;

    SC_CTOR(InnerModule) {
      SC_THREAD(elevate_thread);
    }

  private:
    
    void elevate_thread();
};


#endif 

