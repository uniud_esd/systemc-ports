#ifndef HIERARCHICCONNECTION_HPP
#define HIERARCHICCONNECTION_HPP

#include "simpleconnection.hpp"

SC_MODULE(OuterModule) {

    sc_port<sc_fifo_in_if<unsigned> > din;
    sc_port<sc_fifo_out_if<unsigned> > dout;
    sc_fifo<unsigned> dint;
    
    InnerModule inner1, inner2;
    
    SC_CTOR(OuterModule) : inner1("inner1"), inner2("inner2") {
        inner1.din(this->din);
        inner1.dout(this->dint);
        inner2.din(this->dint);
        inner2.dout(this->dout);
    }
};


#endif 

