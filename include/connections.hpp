#ifndef CONNECTIONS_HPP
#define CONNECTIONS_HPP

#include <systemc.h>

// Typedefs to match diagram and simplify code
typedef sc_signal_in_if<int>    if1;
typedef sc_signal_in_if<int>    if2;
typedef sc_signal_out_if<int>   if3;
typedef sc_signal_inout_if<int> if4;
typedef sc_signal_out_if<int>   if5;
typedef sc_signal_out_if<int>   if6;
typedef sc_signal_in_if<int>    ifB;
typedef sc_signal_in_if<int>    ifD;
typedef sc_signal_in_if<int>    ifF;
typedef sc_signal_in_if<int>    ifW;
typedef sc_signal_in_if<int>    ifX;
typedef sc_signal_in_if<int>    ifY;
typedef sc_signal_in_if<int>    ifZ;
typedef sc_signal<int>          ch1;
typedef sc_signal<int>          ch2;
typedef sc_signal<int>          ch3;
typedef sc_signal<int>          chC;
typedef sc_signal<int>          chG;

SC_MODULE(M1), public ifW {
  sc_port<if1> pA;
  sc_port<ifB> pB;
  sc_export<ifD> pC;
  sc_export<if6> pG;
  
  chC cCi;
  chG cGi;

  SC_CTOR(M1) : pA("pA"), pB("pB"), pC("pC"), pG("pG"), cCi("cCi"), cGi("cGi") {
    pC(cCi);
    pG(cGi);
    SC_THREAD(m1_thread);
  }
  
  void m1_thread(void) { }

  // Methods from sc_signal_in_if<int>
  const sc_event& value_changed_event() const { }
  const int& read() const { }
  const int& get_data_ref() const { }
  bool event() const { }
};

SC_MODULE(M2) {
  sc_port<ifD,2> pD;
  sc_port<if3>   pE;
  sc_port<ifF>   pF;
  
  SC_CTOR(M2) : pD("pD"), pE("pE"), pF("pF") {
    SC_THREAD(m2_thread);
  }
  
  void m2_thread(void) { }
};

SC_MODULE(Peer) {
  sc_port<if5> pJ;
  
  SC_CTOR(Peer) : pJ("pJ") {
    SC_THREAD(peer_thread);
  }
  
  void peer_thread() { }
};

SC_MODULE(Top) {
  sc_port<if1>   p1;
  sc_port<if2>   p2;
  sc_port<if3>   p3;
  sc_port<if4>   p4;
  sc_export<if5> p5;
  sc_export<if6> p6;

  ch1 c1i;
  ch2 c2i;
  ch3 c3i;
  
  M1 mi1;
  M2 mi2;

  sc_event ev1;
  sc_event ev2;
  
  SC_HAS_PROCESS(Top);
  Top(sc_module_name nm) : p1("p1"), p2("p2"), p3("p3"), p4("p4"), p5("p5"), p6("p6"), 
                           c1i("c1i"), c2i("c2i"), c3i("c3i"), mi1("mi1"), mi2("mi2"), sc_module(nm) {
      SC_THREAD(pr1);
      SC_THREAD(pr2);
      SC_THREAD(pr3);

      mi1.pA(p1);
      mi1.pB(c1i);
      mi2.pD(c1i);
      mi2.pD(mi1.pC);
      mi2.pE(p3);
      mi2.pF(c3i);
      p5(c3i);
      p6(mi1.pG);

      // Connections to processes are implicit by way of
      // their usage within the processes themselves.
  }
  
  void pr1() { }
  void pr2() { }
  void pr3() { }
};

#endif
