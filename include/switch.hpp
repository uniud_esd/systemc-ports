#ifndef SWITCH_HPP
#define SWITCH_HPP

SC_MODULE(Switch) 
{
  sc_port<sc_fifo_in_if<int>,4> T1_ip;
  sc_port<sc_signal_out_if<bool>,0> request_op;
  
  SC_CTOR(Switch) {
    SC_THREAD(switch_thread);
  }

  void switch_thread(void);
};

#endif
