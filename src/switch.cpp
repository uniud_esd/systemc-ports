//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <systemc.h>
#include "switch.hpp"

using namespace std;

void Switch::switch_thread(void) 
{
  for (unsigned i=0;i!=request_op.size();i++) 
    request_op[i]->write(true);
  
  wait(T1_ip[0]->data_written_event()
      |T1_ip[1]->data_written_event()
      |T1_ip[2]->data_written_event()
      |T1_ip[3]->data_written_event()
  );
  cout << "INFO(switch_thread): "
       << "Starting up at " << sc_time_stamp() 
       << endl;
  while(true) {
    for(unsigned i=0;i!=T1_ip.size();i++) {
      int value = T1_ip[i]->read(); // Given the order of writes, we will block for some time
      cout << "INFO(switch_thread): "
           << "Read 0x" << hex << value
           << " at " << sc_time_stamp() << endl;
    }
  }
}
