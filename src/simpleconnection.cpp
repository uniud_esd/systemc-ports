#include <systemc.h>
#include "simpleconnection.hpp"

void InnerModule::elevate_thread() {

  while(true) {

    unsigned data = din->read();    
    unsigned result = (1<<data);
    dout->write(result);
  }
}


