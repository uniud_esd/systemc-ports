#include <systemc.h>
#include "export.hpp"

using namespace std;

void ExportInnerModule::elevate_thread() {

  while(true) {
    wait();
    unsigned data = din->read();    
    unsigned result = (1<<data);
    cout << sc_time_stamp() << ": written " << result << endl;
    dout_ch.write(result);
  }
}


